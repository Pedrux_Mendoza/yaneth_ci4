<?php 

namespace App\Entities;
use CodeIgniter\Entity;

class Logeo extends Entity 
{
    private $username;
    private $password; 

    /*G&S*/
    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}