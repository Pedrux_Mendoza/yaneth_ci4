<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\Logeo;
use App\Models\LoginM;

class Login extends Controller
{
	public function __construct()
	{  
		/*print_r($config);*/
		$session = \Config\Services::session();
		/*$session->start(); */   
	}


	public function index()
	{
		return view('login/login');
	}

	//--------------------------------------------------------------------

	public function logon()
	{
		return view('login/access');
	}

	//--------------------------------------------------------------------

	public function iniciar()
	{
		$login = new LoginM();

		//Crear Logeo
		$logeo = new Logeo();
		$logeo->setUsername($this->request->getPost('usuario'));
		$logeo->setPassword(md5($this->request->getPost('pass')));

		$data = $login->validar($logeo);

		if($data){
			echo "<script>alert('Bienvenido!!!!')</script>";
			//variables de sesion
			$session = \Config\Services::session();
			foreach ($data as $dat) {
				$newdata = [
					'username'  => $dat->getUsername(),
					'pass'     => $dat->getPassword(),
					'logueado' => TRUE
				];
			}

			$session->set($newdata);
			return redirect()->to('/login/public/Login/logon');
		}else{
			echo "<script>alert('Error al ingresar: No esta en la base de datos')</script>";
			return redirect()->to('/login/public/Login/index');
		}
	}

	//Metodo para cerrar sesion y destruir la variable de sesion
	public function cerrar(){

		$session = \Config\Services::session();
		$array_items = ['username', 'pass'];
		$session->remove($array_items);
		$newdata = [
			'logueado' => FALSE
		];

		$session->set($newdata);
		$session->destroy();
		return redirect()->to('/login/public/Login/index');
	}

}
