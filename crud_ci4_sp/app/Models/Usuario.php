<?php 

namespace App\Models;
use CodeIgniter\Model;

class Usuario extends Model
{

	protected $table = 'usuario';
	protected $primaryKey = 'id_usuario';

	//--------------------------------------------------------------------

	public function getUsuarios()
	{
		$sp_consultar_uc = 'CALL sp_consultar_uc()';
		$query = $this->query($sp_consultar_uc);

		return $query->getResult();
	}

	//--------------------------------------------------------------------

	public function eliminar($id)
	{
		$sp_eliminar_usuario = 'CALL sp_eliminar_usuario(?)';
		$query = $this->query($sp_eliminar_usuario, [$id]);

		if ($query) {
			return true;
		}else{
			return false;
		}
	}

	//--------------------------------------------------------------------

	public function getCompetencia()
	{
		$sp_consultar_competencias = 'CALL sp_consultar_competencias()';
		$query = $this->query($sp_consultar_competencias);

		return $query->getResult();
	}

	//--------------------------------------------------------------------


	public function insertar($datos)
	{
		$sp_insertar_usuario = 'CALL sp_insertar_usuario(?, ?, ?, ?)';
		$query = $this->query($sp_insertar_usuario, [$datos['usuario'],$datos['contraseña'],$datos['correo'],$datos['competencia']]);

		if ($query) {
			return "add";
		}else{
			return "errorA";
		}
	}

	//--------------------------------------------------------------------

	public function getDatos($id)
	{
		$sp_consultarporid = 'CALL sp_consultarporid(?)';
		$query = $this->query($sp_consultarporid, [$id]);

		return $query->getResult();
	}

	//--------------------------------------------------------------------

	public function actualizar($datos)
	{
		$sp_modificar_usuario = 'CALL sp_modificar_usuario(?, ?, ?, ?)';
		$query = $this->query($sp_modificar_usuario, [$datos['id'],$datos['usuario'],$datos['correo'],$datos['competencia']]);

		if ($query) {
			return "modi";
		}else{
			return "errorM";
		}
	}	

}